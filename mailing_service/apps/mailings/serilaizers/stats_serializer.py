from rest_framework import serializers

from apps.commons.consts.mailing_status import MAILING_STATUS


class DetailStatSerializer(serializers.Serializer):
    """Сериализация для детальной статистики отправленных сообщений по конкретной рассылке"""

    def __init__(self, *args, **kwargs):
        super(DetailStatSerializer, self).__init__(*args, **kwargs)
        for status in MAILING_STATUS:
            self.fields[status[0]] = serializers.IntegerField(label=f'Число сообщений со статусом "{status[1]}"')


class MainStatSerializer(DetailStatSerializer):
    """
        Сериализация для общей статистики по созданным рассылкам и количеству
        отправленных сообщений по ним с группировкой по статусам
    """
    mailings_count = serializers.IntegerField(label='Общее число рассылок')
