from rest_framework import serializers

from apps.mailings.models import Mailing


class MailingSerializer(serializers.ModelSerializer):
    """Сериализация модели рассылок"""

    time_start = serializers.DateTimeField(
        input_formats=['%d.%m.%Y %H:%M', ]
    )
    time_stop = serializers.DateTimeField(
        input_formats=['%d.%m.%Y %H:%M', ]
    )

    class Meta:
        model = Mailing
        exclude = ('date_create', )
