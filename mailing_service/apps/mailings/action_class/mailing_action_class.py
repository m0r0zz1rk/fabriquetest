import datetime
import logging

from apps.commons.base_classes.base_action_class import BaseActionClass
from apps.commons.consts.mailing_filter import TAG
from apps.commons.exception import ExceptionHandling
from apps.mailings.models import Mailing, generate_time_stop
from apps.mailings.utils.mailing_utils import MailingUtils


class MailingActionClass(BaseActionClass):
    """Класс действий с моделью рассылок"""

    base_object = {
        'mailing_id': None,
        'time_start': datetime.datetime.now,
        'message_text': 'Текст сообщения',
        'filter': TAG,
        'filter_value': 'Образование',
        'time_stop': generate_time_stop()
    }

    def _set_logger(self):
        """Абстрактный метод из базового класса"""
        self.logger = logging.getLogger('Mailing')

    def _set_model(self):
        """Абстрактный метод из базового класса"""
        self.model = Mailing

    def _set_field_id(self):
        """Абстрактный метод из базового класса"""
        self.field_id = 'mailing_id'

    def _set_get_object_parameter_method(self):
        """Абстрактный метод из базового класса"""
        self.get_object_parameter_method = MailingUtils().get_mailing_attribute_by_mailing_id

    def _create_success_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Рассылка с началом {self.base_object["time_start"]} ' \
               f'и сообщением {self.base_object["message_text"]} успешно создана'

    def _create_error_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Произошла ошибка при создании рассылки: {ExceptionHandling.get_traceback()}'

    def _update_no_id_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return 'Не указан идентификатор рассылки для обновления данных'

    def _update_not_found_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return 'Рассылка по предъявленному идентификатору не найдена'

    def _update_success_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Рассылка с началом {self.base_object["time_start"]} ' \
               f'и сообщением {self.base_object["message_text"]} успешно обновлена'

    def _update_error_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Произошла ошибка при обновлении рассылки: {ExceptionHandling.get_traceback()}'

    def _delete_success_message(self, data: dict) -> str:
        """Абстрактный метод из базового класса"""
        time_start = MailingUtils().get_mailing_attribute_by_mailing_id(data["mailing_id"], 'time_start')
        message_text = MailingUtils().get_mailing_attribute_by_mailing_id(data["mailing_id"], 'message_text')
        return f'Рассылка с началом {time_start} ' \
               f'и сообщением {message_text} успешно удалена'

    def _delete_error_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Произошла ошибка при удалении рассылки: {ExceptionHandling.get_traceback()}'

    def _delete_no_id_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Не указан идентификатор рассылки для удаления'
