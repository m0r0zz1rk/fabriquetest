from django.apps import AppConfig


class MailingsConfig(AppConfig):
    name = 'apps.mailings'
    verbose_name = 'Приложение рассылок'

