import datetime
import logging

import pytz

from apps.celery_app.tasks import send_mailing_messages_task
from apps.mailings.service.mailings_actions.ExternalServiceClass import ExternalServiceAction
from apps.mailings.utils.mailing_utils import MailingUtils
from apps.mailings_messages.models import Message


class MailingStartClass:
    """Класс для запуска рассылок"""

    logger = mailing_id = time_start = time_stop = None

    def __init__(self, mailing_id: int):
        """Инициализация - установка ID рассылки и времени ее запуска"""
        self.mailing_id = mailing_id
        self._set_logger()
        self._set_time_start_and_stop()

    def _set_logger(self):
        """Установка журнала"""
        self.logger = logging.getLogger('MailingStart')

    def _set_time_start_and_stop(self):
        """Установка времени начала и окончания рассылки"""
        self.time_start = MailingUtils().get_mailing_attribute_by_mailing_id(
            self.mailing_id,
            'time_start'
        )
        self.time_stop = MailingUtils().get_mailing_attribute_by_mailing_id(
            self.mailing_id,
            'time_stop'
        )

    def check_mailing_start(self, from_unit_test=False):
        """Проверка на время запуска рассылки"""
        if self.time_start.replace(tzinfo=pytz.UTC) <= \
                datetime.datetime.now().replace(tzinfo=pytz.UTC) <= \
                self.time_stop.replace(tzinfo=pytz.UTC) and \
                not Message.objects.filter(mailing_id=self.mailing_id).exists():
            self.logger.info(f'Запущена рассылка с ID {self.mailing_id}')
            if not from_unit_test:
                send_mailing_messages_task.delay(self.mailing_id)
            else:
                ExternalServiceAction(self.mailing_id).mailing()
        else:
            pass
