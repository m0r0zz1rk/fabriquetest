import logging

import requests

from apps.clients.utils.client_utils import ClientUtils
from apps.commons.consts.mailing_status import MAILING_PREPARE, MAILING_PROCESS, MAILING_STATUS, MAILING_SUCCESS, \
    MAILING_ERROR
from apps.commons.django import DjangoUtils
from apps.mailings.utils.mailing_utils import MailingUtils
from apps.mailings_messages.models import Message
from apps.mailings_messages.utils.mailing_message_utils import MailingMessageUtils


class ExternalServiceAction:
    """Класс для работы с внешним сервисом отправки сообщений"""
    name = 'apps.mailings.service.celery.ExternalServiceAction.ExternalServiceAction'

    jwt_fabrique = DjangoUtils().get_parameter_from_settings('JWT_FABRIQUE')

    endpoint = 'https://probe.fbrq.cloud/v1/send/'
    headers = {
        'Accept': 'application/json',
        'Authorization': f"Bearer {jwt_fabrique}",
        'Content-Type': 'application/json',
    }
    timeout = DjangoUtils().get_parameter_from_settings('TIMEOUT_FABRIQUE')

    response = None
    clients = ()
    data = {}
    text_message = ''
    logger = new_message = mailing_id = client_id = None

    def __init__(self, mailing_id: int):
        """Инициализация - установка журнала, ID рассылки, получение списка клиентов для рассылки"""
        self._set_logger()
        self.mailing_id = mailing_id
        self._get_text_message()
        self._get_clients_for_mailing()

    def _set_logger(self):
        """Установка журнала"""
        self.logger = logging.getLogger('ExternalService')

    def _get_text_message(self):
        """Установка текста сообщения рассылки"""
        self.text_message = MailingUtils().get_mailing_attribute_by_mailing_id(
            self.mailing_id,
            'message_text'
        )

    def _get_clients_for_mailing(self):
        """Получение списка клиентов, подходящих под фильтр рассылки"""
        self.clients = MailingUtils().get_clients_for_mailing(self.mailing_id)

    def _create_new_message(self):
        """Создание нового сообщения"""
        self.new_message = Message.objects.create(
            mailing_status=MAILING_PREPARE,
            mailing_id=self.mailing_id,
            client_id=self.client_id
        )

    def _set_request_data(self):
        """Установка значений тела запроса"""
        self.data = {
            'id': int(self.new_message.message_id),
            'phone': int(ClientUtils().get_client_attribute_by_client_id(
                self.new_message.client_id,
                'phone'
            )),
            'text': self.text_message
        }

    def _update_message_status_process(self, status: MAILING_STATUS):
        """Установка сообщению полученного статуса"""
        MailingMessageUtils().set_mailing_message_status(self.new_message.message_id, status)

    def _generator_clients(self):
        """Генератор для получения клиента из списка"""
        for client in self.clients:
            yield client

    def _send_request(self):
        """Отправка запроса на внешний сервис"""
        self.response = requests.post(
            f'{self.endpoint}{self.new_message.message_id}',
            json=self.data,
            headers=self.headers,
            timeout=self.timeout
        )

    def mailing(self):
        """Рассылка сообщений"""
        if len(self.clients) != 0:
            cnt = 0
            clients_gen = self._generator_clients()
            while cnt < len(self.clients):
                self.client_id = clients_gen.__next__()
                self._create_new_message()
                self._set_request_data()
                self._update_message_status_process(MAILING_PROCESS)
                self._send_request()
                if self.response.status_code == 200:
                    self._update_message_status_process(MAILING_SUCCESS)
                else:
                    self._update_message_status_process(MAILING_ERROR)
                cnt += 1
