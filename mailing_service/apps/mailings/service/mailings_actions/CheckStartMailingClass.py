from apps.mailings.service.MailingStartClass import MailingStartClass
from apps.mailings.utils.mailing_utils import MailingUtils


class CheckStartMailingsAction:
    """Класс проверки рассылок на запуск"""
    name = 'apps.mailings.service.celery.CheckStartMailingClass.CheckStartMailingsAction'
    mailings = []

    def __init__(self):
        """Ининциализация - установка списка ID рассылок для проверки"""
        self._get_mailings()

    def _get_mailings(self):
        """Получение списка ID рассылок, время окончания которых не наступило"""
        self.mailings = MailingUtils.get_list_mailings_id_for_check()

    def _generator_mailing(self):
        """Генератор для прохождения списка рассылок"""
        for mailing in self.mailings:
            yield mailing

    def start_mailings(self, from_unit_test=False):
        """Отправка рассылок из списка в класс запуска рассылок"""
        gen = self._generator_mailing()
        iterator = 0
        while iterator < len(self.mailings):
            mailing_id = gen.__next__()
            MailingStartClass(mailing_id).check_mailing_start(from_unit_test=from_unit_test)
            iterator += 1
