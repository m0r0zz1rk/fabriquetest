from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets

from apps.celery_app.tasks import check_and_start_mailing_task
from apps.commons.dict import DictUtils
from apps.commons.response import ResponsesClass
from apps.mailings.serilaizers.stats_serializer import MainStatSerializer, DetailStatSerializer
from apps.mailings.utils.mailing_utils import MailingUtils


class MailingStatsViewSet(viewsets.ViewSet):
    """Эндпоинт для получения статистик по рассылкам"""

    @staticmethod
    @swagger_auto_schema(
        tags=['Рассылки (статистика)', ],
        operation_description="Получение общей статистики по созданным рассылкам и количеству "
                              "отправленных сообщений по ним с группировкой по статусам",
        responses={
            '200': MainStatSerializer,
            '400': 'Произошла ошибка в процессе формирования статистики'
        }
    )
    def get_main_statistic(request, *args, **kwargs):
        """
            Получение общей статистики по созданным рассылкам и количеству отправленных сообщений
            по ним с группировкой по статусам
        """
        check_and_start_mailing_task()
        data = MailingUtils.get_main_statistics()
        serialize = MainStatSerializer(data=data)
        if serialize.is_valid(raise_exception=True):
            return ResponsesClass.ok_response_dict(serialize.data)
        else:
            return ResponsesClass.bad_request_response(serialize.errors)

    @staticmethod
    @swagger_auto_schema(
        tags=['Рассылки (статистика)', ],
        manual_parameters=[
            openapi.Parameter(
                'mailing_id',
                openapi.IN_QUERY,
                description='ID рассылки',
                type=openapi.TYPE_INTEGER,
                required=True
            )
        ],
        operation_description="Получение детальной статистики отправленных сообщений по конкретной рассылке",
        responses={
            '200': DetailStatSerializer,
            '400': 'Произошла ошибка в процессе формирования статистики',
            '404': 'Не указан идентификатор рассылки'
        }
    )
    def get_detail_statistic(request, *args, **kwargs):
        """
            Получение детальной статистики отправленных сообщений по конкретной рассылке
        """
        du = DictUtils()
        resu = ResponsesClass()
        mailing_id = du.get_int_value_in_dict_by_key('mailing_id', kwargs)
        if mailing_id is None:
            return resu.not_found_response('Не указан идентификатор рассылки')
        data = MailingUtils().get_detail_statistic(mailing_id)
        serialize = DetailStatSerializer(data=data)
        if serialize.is_valid(raise_exception=True):
            return ResponsesClass.ok_response_dict(serialize.data)
        else:
            return ResponsesClass.bad_request_response(serialize.errors)
