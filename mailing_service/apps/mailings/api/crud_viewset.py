import logging

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets

from apps.commons.consts.action_status import SUCCESS
from apps.commons.consts.actions import CREATE, UPDATE, DELETE
from apps.commons.dict import DictUtils
from apps.commons.exception import ExceptionHandling
from apps.commons.response import ResponsesClass
from apps.commons.rest import RestUtils
from apps.mailings.action_class.mailing_action_class import MailingActionClass
from apps.mailings.serilaizers.mailing_serializer import MailingSerializer


class MailingCRUDViewSet(viewsets.ViewSet):
    """Эндпоинт для CRUD-операций с моделью рассылок"""

    ru = RestUtils()
    du = DictUtils()
    resu = ResponsesClass()
    logger = logging.getLogger('MailingAPI')

    list_create_params = ['time_start', 'message_text', 'filter', 'filter_value', 'time_stop']

    @swagger_auto_schema(
        tags=['Рассылки', ],
        request_body=MailingSerializer,
        operation_description="Добавление рассылки",
        responses={'400': 'Произошла ошибка в процессе добавления (запись в лог)',
                   '200': 'Рассылка успешно создана"'}
    )
    def create(self, request, *args, **kwargs):
        """Создание рассылки"""
        if not self.ru.validate_params_to_list(request, self.list_create_params):
            self.logger.error('Тело запроса не прошло валидацию')
            return self.resu.bad_request_response('Данные введены некорректно')
        serialize = MailingSerializer(data=request.data)
        if serialize.is_valid(raise_exception=True):
            if MailingActionClass().action(CREATE, serialize.data) is SUCCESS:
                self.logger.info('Рассылка создана')
                return self.resu.ok_response('Рассылка успешно создана')
            self.logger.error(f'Ошибка при добавлении рассылки')
            return self.resu.sorry_try_again_response()
        self.logger.error(f'Ошибка сериализации данных о рассылке при добавлении: {ExceptionHandling.get_traceback()}')
        return self.resu.sorry_try_again_response()

    @swagger_auto_schema(
        tags=['Рассылки', ],
        manual_parameters=[
            openapi.Parameter(
                'mailing_id',
                openapi.IN_QUERY,
                description='ID рассылки',
                type=openapi.TYPE_INTEGER,
                required=True
            )
        ],
        request_body=MailingSerializer,
        operation_description="Обновление рассылки",
        responses={
            '200': 'Сообщение "Рассылка успешно обновлена"',
            '400': 'Произошла ошибка в процессе обновления (запись в лог)',
            '404': 'Не указан идентификатор рассылки'
        }
    )
    def update(self, request, *args, **kwargs):
        """Обновление рассылки"""
        mailing_id = self.du.get_int_value_in_dict_by_key('mailing_id', kwargs)
        if mailing_id is None:
            self.logger.error('Не получен идентификатор рассылки для обновления')
            return self.resu.not_found_response('Не указан идентификатор рассылки')
        if not self.ru.validate_params_to_list(request, self.list_create_params):
            self.logger.error('Тело запроса не прошло валидацию')
            return self.resu.bad_request_response('Данные введены некорректно')
        serialize = MailingSerializer(data=request.data)
        if serialize.is_valid(raise_exception=True):
            try:
                full_data = {
                    'mailing_id': mailing_id
                }
                full_data.update(serialize.data)
                if MailingActionClass().action(UPDATE, full_data) is SUCCESS:
                    self.logger.info('Рассылка обновлена')
                    return self.resu.ok_response('Рассылка успешно обновлена')
                self.logger.error(f'Ошибка при обновлении рассылки')
                return self.resu.sorry_try_again_response()
            except Exception:
                self.logger.error(f'Ошибка при обновлении данных о рассылке: {ExceptionHandling.get_traceback()}')
                return self.resu.sorry_try_again_response()
        else:
            self.logger.error(f'Ошибка сериализации данных о рассылке при обновлении: {ExceptionHandling.get_traceback()}')
            return self.resu.sorry_try_again_response()

    @swagger_auto_schema(
        tags=['Рассылки', ],
        manual_parameters=[
            openapi.Parameter(
                'mailing_id',
                openapi.IN_QUERY,
                description='ID рассылки',
                type=openapi.TYPE_INTEGER,
                required=True
            )
        ],
        operation_description="Удаление рассылки",
        responses={
            '200': 'Сообщение "Рассылка успешно удалена"',
            '400': 'Произошла ошибка в процессе удаления (запись в лог)',
            '404': 'Не указан идентификатор рассылки'
        }
    )
    def destroy(self, request, *args, **kwargs):
        """Удаление рассылки"""
        mailing_id = self.du.get_int_value_in_dict_by_key('mailing_id', kwargs)
        if mailing_id is None:
            self.logger.error('Не получен идентификатор рассылки для обновления')
            return self.resu.not_found_response('Не указан идентификатор рассылки')
        try:
            if MailingActionClass().action(DELETE, {'mailing_id': mailing_id}) is SUCCESS:
                self.logger.info('Рассылка удалена')
                return self.resu.ok_response('Рассылка успешно удалена')
            self.logger.error('Ошибка при удалении рассылки')
            return self.resu.sorry_try_again_response()
        except Exception:
            self.logger.error(f'Ошибка при удалении рассылки: {ExceptionHandling.get_traceback()}')
            return self.resu.sorry_try_again_response()
