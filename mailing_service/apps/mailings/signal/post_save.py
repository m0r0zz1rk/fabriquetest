from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.celery_app.tasks import check_and_start_mailing_task
from apps.mailings.models import Mailing


@receiver(post_save, sender=Mailing)
def check_for_mailing_start(sender, instance, created, **kwargs):
    """
        Проверка рассылки на время старта, в случае, если время запуска прошло и окончания - не наступило, запустить
        рассылку
    """
    check_and_start_mailing_task.delay()
