from django.contrib import admin

from apps.mailings.models import Mailing


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    """Добавление модели рассылок в административную панель"""
    pass
