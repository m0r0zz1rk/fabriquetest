import datetime

from django.db import models

from apps.commons.consts.mailing_filter import MAILING_FILTER
from apps.commons.django import DjangoUtils
from apps.commons.models import BaseTable


def generate_time_stop():
    time_delta = DjangoUtils().get_parameter_from_settings('DEFAULT_DELTA_MINUTES_TIME_STOP')
    if time_delta is not None:
        return datetime.datetime.now() + datetime.timedelta(minutes=time_delta)
    return datetime.datetime.now() + datetime.timedelta(minutes=15)


class Mailing(BaseTable):
    """Модель рассылок"""
    mailing_id = models.AutoField(
        primary_key=True,
        editable=False,
        verbose_name='ID рассылки'
    )
    time_start = models.DateTimeField(
        null=True,
        default=datetime.datetime.now,
        verbose_name='Время запуска рассылки'
    )
    message_text = models.TextField(
        verbose_name='Текст сообщения'
    )
    filter = models.CharField(
        max_length=25,
        choices=MAILING_FILTER,
        verbose_name='Фильтр свойств клиентов'
    )
    filter_value = models.CharField(
        max_length=11,
        verbose_name='Значение фильтра'
    )
    time_stop = models.DateTimeField(
        default=generate_time_stop,
        verbose_name='Время окончания рассылки'
    )

    def __str__(self):
        return f'Рассылка с началом {self.time_start.strftime("%d.%m.%Y %H:%M")} до ' \
                f'{self.time_stop.strftime("%d.%m.%Y %H:%M")} для клиентов с атрибутом "{self.filter}" равным ' \
                f'"{self.filter_value}", сообщение: {self.message_text}'

    class Meta:
        ordering = ['-time_start']
        indexes = [
            models.Index(fields=['message_text'])
        ]
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'
