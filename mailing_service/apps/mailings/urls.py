from django.urls import path

from apps.mailings.api.crud_viewset import MailingCRUDViewSet
from apps.mailings.api.stats_viewset import MailingStatsViewSet

crud_urlpatterns = [
    path('new/', MailingCRUDViewSet.as_view({'post': 'create'})),
    path('update/<int:mailing_id>/', MailingCRUDViewSet.as_view({'put': 'update'})),
    path('delete/<int:mailing_id>/', MailingCRUDViewSet.as_view({'delete': 'destroy'}))
]

stats_urlpatterns = [
    path('main_stat/', MailingStatsViewSet.as_view({'get': 'get_main_statistic'})),
    path('detail_stat/<int:mailing_id>/', MailingStatsViewSet.as_view({'get': 'get_detail_statistic'}))
]

urlpatterns = crud_urlpatterns + stats_urlpatterns
