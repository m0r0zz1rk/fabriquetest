import json

from django.test import TestCase
from rest_framework.test import RequestsClient

from apps.clients.models import Client
from apps.commons.django import DjangoUtils
from apps.mailings.models import Mailing


class APITests(TestCase):
    """Класс тестирования API"""

    @classmethod
    def setUpTestData(cls):
        """Создание тестовой рассылки"""
        data = {
            'time_start': '2023-09-12 12:00',
            'message_text': 'Сообщение для рассылки из тестирования',
            'filter': 'tag',
            'filter_value': 'Образование',
            'time_stop': '2023-12-12 12:00'
        }
        Mailing.objects.create(
            **data
        )

    def setUp(self):
        """Установка URL для обращения к API, экземпляра класса RequestsClient"""
        self.url = DjangoUtils().get_parameter_from_settings('TEST_URL')
        self.client = RequestsClient()
        self.client.headers.update({'Content-Type': 'application/json'})

    def testNewMailing(self):
        """Тестирование API на добавление новой рассылки"""
        data = {
            'time_start': '13.09.2023 12:30',
            'message_text': 'Сообщение для рассылки из тестирования #2',
            'filter': 'phone_code',
            'filter_value': 850,
            'time_stop': '13.12.2023 12:30'
        }
        response = self.client.post(
            f'{self.url}/api/v1/mailings/new/',
            data=json.dumps(data)
        )
        assert response.status_code == 200
        assert Mailing.objects.count() == 2

    def testUpdateMailing(self):
        """Тестирование API на обновление данных рассылки"""
        data = {
            'time_start': '12.09.2023 13:50',
            'message_text': 'Сообщение для рассылки из тестирования',
            'filter': 'tag',
            'filter_value': 'Вероятность',
            'time_stop': '12.12.2023 13:50'
        }
        response = self.client.put(
            f'{self.url}/api/v1/mailings/update/1/',
            data=json.dumps(data)
        )
        assert response.status_code == 200
        assert Mailing.objects.get(mailing_id=1).filter_value == 'Вероятность'

    def testDeleteMailing(self):
        """Тестирование API на удаление рассылки"""
        response = self.client.delete(
            f'{self.url}/api/v1/mailings/delete/1/'
        )
        assert response.status_code == 200
        assert Mailing.objects.count() == 0
