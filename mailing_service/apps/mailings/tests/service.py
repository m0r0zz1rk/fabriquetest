import datetime

import pytz
from django.test import TestCase

from apps.clients.models import Client
from apps.mailings.models import Mailing
from apps.mailings.service.mailings_actions.CheckStartMailingClass import CheckStartMailingsAction
from apps.mailings_messages.models import Message


class ServiceTests(TestCase):
    """Класс тестирования функций по работе с рассылками"""

    @classmethod
    def setUpTestData(cls):
        """Создание тестовых клиентов для проверки корректности отправки сообщений по рассылке"""
        data = {
            'phone': '78005553530',
            'phone_code': 800,
            'tag': 'Тестирование',
            'timezone': 'Europe/Moscow'
        }
        Client.objects.create(
            **data
        )
        data = {
            'phone': '78505553521',
            'phone_code': 850,
            'tag': 'Тестирование',
            'timezone': 'Europe/Moscow'
        }
        Client.objects.create(
            **data
        )
        data = {
            'phone': '78505553549',
            'phone_code': 850,
            'tag': 'Образование',
            'timezone': 'Europe/Moscow'
        }
        Client.objects.create(
            **data
        )

    @staticmethod
    def testMailingFilterMessages():
        """Тестирование отправки сообщений по заданным параметрам"""
        data = {
            'time_start': datetime.datetime.now().replace(tzinfo=pytz.UTC) - datetime.timedelta(minutes=15),
            'message_text': 'Сообщение для рассылки из тестирования #2',
            'filter': 'tag',
            'filter_value': 'Образование',
            'time_stop': datetime.datetime.now().replace(tzinfo=pytz.UTC) + datetime.timedelta(minutes=15)
        }
        mailing1 = Mailing.objects.create(**data)
        data = {
            'time_start': datetime.datetime.now().replace(tzinfo=pytz.UTC) - datetime.timedelta(minutes=15),
            'message_text': 'Сообщение для рассылки из тестирования #2',
            'filter': 'phone_code',
            'filter_value': 850,
            'time_stop': datetime.datetime.now().replace(tzinfo=pytz.UTC) + datetime.timedelta(minutes=15)
        }
        mailing2 = Mailing.objects.create(**data)
        CheckStartMailingsAction().start_mailings(from_unit_test=True)
        assert Message.objects.filter(mailing_id=mailing1.mailing_id).count() == 1
        assert Message.objects.filter(mailing_id=mailing2.mailing_id).count() == 2

    @staticmethod
    def testTimeStartMailing():
        """Тестирование проверки времени запуска и остановки рассылки"""
        data = {
            'time_start': datetime.datetime.now().replace(tzinfo=pytz.UTC) - datetime.timedelta(minutes=15),
            'message_text': 'Сообщение для рассылки из тестирования #2',
            'filter': 'tag',
            'filter_value': 'Образование',
            'time_stop': datetime.datetime.now().replace(tzinfo=pytz.UTC) - datetime.timedelta(minutes=5)
        }
        mailing1 = Mailing.objects.create(**data)
        CheckStartMailingsAction().start_mailings(from_unit_test=True)
        assert Message.objects.filter(mailing_id=mailing1.mailing_id).count() == 0
