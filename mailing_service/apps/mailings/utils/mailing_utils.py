import datetime

import pytz

from apps.clients.models import Client
from apps.commons.consts.mailing_status import MAILING_STATUS
from apps.mailings.models import Mailing
from apps.mailings_messages.models import Message


class MailingUtils:
    """Класс методов для работы с рассылками"""

    @staticmethod
    def check_mailing_by_mailing_id(mailing_id: int) -> bool:
        """Проверка на существующую рассылку по полученному mailing_id"""
        return Mailing.objects.filter(mailing_id=mailing_id).exists()

    def get_mailing_attribute_by_mailing_id(self, mailing_id: int, parameter: str):
        """Получение атрибута рассылки по mailing_id"""
        if self.check_mailing_by_mailing_id(mailing_id):
            try:
                return getattr(Mailing.objects.get(mailing_id=mailing_id), parameter)
            except:
                pass
        return None

    def get_clients_for_mailing(self, mailing_id: int) -> list:
        """Получение списка ID клиентов, подходящих под фильтр рассылки"""
        clients = ()
        if self.check_mailing_by_mailing_id(mailing_id):
            filter_field = self.get_mailing_attribute_by_mailing_id(
                mailing_id,
                'filter'
            )
            filter_value = self.get_mailing_attribute_by_mailing_id(
                mailing_id,
                'filter_value'
            )
            orm_filter = {
                filter_field: filter_value
            }
            if Client.objects.filter(**orm_filter).exists():
                clients = [client.client_id for client in Client.objects.filter(**orm_filter)]
        return clients

    @staticmethod
    def get_list_mailings_id_for_check() -> list:
        """Получение списка ID рассылок, время окончания которых не наступило"""
        mailings = []
        if Mailing.objects.filter(time_stop__gt=datetime.datetime.now().strftime('%Y-%m-%d %H:%M')).exists():
            mailings = [mailing.mailing_id for mailing in Mailing.objects.filter(
                time_stop__gt=datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
            ) if not Message.objects.filter(mailing_id=mailing.mailing_id).exists()]
        return mailings

    @staticmethod
    def get_main_statistics(for_email=False) -> dict:
        """
            Получение общей статистики по созданным рассылкам и количеству отправленных сообщений
            по ним с группировкой по статусам
        :param for_email: Флаг - статистика для email сообщения
        :return: словарь
        """
        stats = {
            'mailings_count': 0
        }
        for status in MAILING_STATUS:
            stats[status[0]] = 0
        queryset_mailing = Mailing.objects.all()
        if for_email:
            queryset_mailing = queryset_mailing.filter(time_stop__lte=datetime.datetime.now().replace(tzinfo=pytz.UTC))
        if queryset_mailing.exists():
            mailings = [mailing['mailing_id'] for mailing in queryset_mailing.values('mailing_id')]
            queryset_messages = Message.objects.select_related('mailing', 'client').all()
            if for_email:
                queryset_messages = queryset_messages.filter(mailing_id__in=mailings)
            messages = [message['mailing_status'] for message in queryset_messages.values('mailing_status')]
            stats['mailings_count'] = len(mailings)
            for status in MAILING_STATUS:
                stats[status[0]] = messages.count(status[0])
        return stats

    def get_detail_statistic(self, mailing_id: int) -> dict:
        """
            Получение общей статистики по созданным рассылкам и количеству отправленных сообщений
            по ним с группировкой по статусам
        """
        stats = {}
        for status in MAILING_STATUS:
            stats[status[0]] = 0
        if self.check_mailing_by_mailing_id(mailing_id) and \
                Message.objects.select_related('mailing', 'client').filter(mailing_id=mailing_id).exists():
            messages = [message['mailing_status'] for message in \
                        Message.objects.select_related('mailing', 'client').filter(mailing_id=mailing_id). \
                            values('mailing_status')]
            for status in MAILING_STATUS:
                stats[status[0]] = messages.count(status[0])
        return stats
