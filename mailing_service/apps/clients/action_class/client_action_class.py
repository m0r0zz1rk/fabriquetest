import logging

from apps.clients.models import Client
from apps.clients.utils.client_utils import ClientUtils
from apps.commons.base_classes.base_action_class import BaseActionClass
from apps.commons.exception import ExceptionHandling


class ClientActionClass(BaseActionClass):
    """Класс действий с моделью клиентов"""

    base_object = {
        'client_id': None,
        'phone': '88005553535',
        'phone_code': '800',
        'tag': 'Тэг',
        'timezone': 'Europe/Moscow'
    }

    def _set_logger(self):
        """Абстрактный метод из базового класса"""
        self.logger = logging.getLogger('Client')

    def _set_model(self):
        """Абстрактный метод из базового класса"""
        self.model = Client

    def _set_field_id(self):
        """Абстрактный метод из базового класса"""
        self.field_id = 'client_id'

    def _set_get_object_parameter_method(self):
        """Абстрактный метод из базового класса"""
        self.get_object_parameter_method = ClientUtils().get_client_attribute_by_client_id

    def _create_success_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Клиент {self.base_object["phone"]} успешно добавлен'

    def _create_error_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Произошла ошибка при добавлении клиента: {ExceptionHandling.get_traceback()}'

    def _update_no_id_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return 'Не указан идентификатор клиента для обновления данных'

    def _update_not_found_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return 'Клиент по предъявленному идентификатору не найден'

    def _update_success_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Клиент {self.base_object["phone"]} успешно обновлен'

    def _update_error_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Произошла ошибка при обновлении клиента: {ExceptionHandling.get_traceback()}'

    def _delete_success_message(self, data: dict) -> str:
        """Абстрактный метод из базового класса"""
        phone = ClientUtils().get_client_attribute_by_client_id(data["client_id"], 'phone')
        return f'Клиент {phone} успешно удален'

    def _delete_error_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Произошла ошибка при удалении клиента: {ExceptionHandling.get_traceback()}'

    def _delete_no_id_message(self) -> str:
        """Абстрактный метод из базового класса"""
        return f'Не указан идентификатор клиента для удаления'
