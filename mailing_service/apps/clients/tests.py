import json

from django.test import TestCase
from rest_framework.test import RequestsClient

from apps.clients.models import Client
from apps.commons.django import DjangoUtils


class APITests(TestCase):
    """Класс тестирования API"""

    @classmethod
    def setUpTestData(cls):
        """Создание тестового пользователя"""
        data = {
            'phone': '78005553535',
            'phone_code': 800,
            'tag': 'Тестирование',
            'timezone': 'Europe/Moscow'
        }
        Client.objects.create(
            **data
        )

    def setUp(self):
        """Установка URL для обращения к API, экземпляра класса RequestsClient"""
        self.url = DjangoUtils().get_parameter_from_settings('TEST_URL')
        self.client = RequestsClient()
        self.client.headers.update({'Content-Type': 'application/json'})

    def testNewClient(self):
        """Тестирование API на добавление клиента"""
        data = {
            'phone': '75894837483',
            'phone_code': 589,
            'tag': 'Вероятность',
            'timezone': 'Asia/Irkutsk'
        }
        response = self.client.post(
            f'{self.url}/api/v1/clients/new/',
            data=json.dumps(data)
        )
        assert response.status_code == 200
        assert Client.objects.count() == 2

    def testUpdateClient(self):
        """Тестирование API на обновление данных клиента"""
        data = {
            'phone': '75894837483',
            'phone_code': 589,
            'tag': 'Вероятность',
            'timezone': 'Asia/Irkutsk'
        }
        response = self.client.put(
            f'{self.url}/api/v1/clients/update/1/',
            data=json.dumps(data)
        )
        assert response.status_code == 200
        assert Client.objects.get(client_id=1).tag == 'Вероятность'

    def testDeleteClient(self):
        """Тестирование API на удаление клиента"""
        response = self.client.delete(
            f'{self.url}/api/v1/clients/delete/1/'
        )
        assert response.status_code == 200
        assert not Client.objects.exists()
