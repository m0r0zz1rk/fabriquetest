from rest_framework import serializers

from apps.clients.models import Client


class ClientSerializer(serializers.ModelSerializer):
    """Сериализация данных клиента при добавлении"""
    class Meta:
        model = Client
        exclude = ('client_id', 'date_create')
