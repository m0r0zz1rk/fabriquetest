from typing import Optional

from apps.clients.models import Client


class ClientUtils:
    """Класс для работы с моделью клиентов"""

    @staticmethod
    def check_client_by_client_id(client_id: int) -> bool:
        """Проверка на существующего пользователя по полученному client_id"""
        return Client.objects.filter(client_id=client_id).exists()

    def get_client_attribute_by_client_id(self, client_id: int, parameter: str) -> Optional[str]:
        """Получение аттрибута клиента по client_id"""
        if self.check_client_by_client_id(client_id):
            try:
                return getattr(Client.objects.get(client_id=client_id), parameter)
            except:
                pass
        return None

    def get_client_date_create_by_client_id(self, client_id: int) -> Optional[Client]:
        """Получение сущности Клиент по полученному client_id"""
        if self.check_client_by_client_id(client_id):
            return Client.objects.get(client_id=client_id)
        return None
