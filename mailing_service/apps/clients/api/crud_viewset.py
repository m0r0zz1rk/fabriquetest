import logging

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets

from apps.clients.action_class.client_action_class import ClientActionClass
from apps.clients.serializers.client_serializer import ClientSerializer
from apps.commons.consts.action_status import SUCCESS
from apps.commons.consts.actions import CREATE, UPDATE, DELETE
from apps.commons.dict import DictUtils
from apps.commons.exception import ExceptionHandling
from apps.commons.response import ResponsesClass
from apps.commons.rest import RestUtils


class ClientCRUDViewSet(viewsets.ViewSet):
    """Эндпоинт для CRUD-операций с моделью клиентов"""

    ru = RestUtils()
    du = DictUtils()
    resu = ResponsesClass()
    logger = logging.getLogger('ClientAPI')

    list_create_params = ['phone', 'phone_code', 'tag', 'timezone']

    @swagger_auto_schema(
        tags=['Клиенты', ],
        request_body=ClientSerializer,
        operation_description="Добавление клиента",
        responses={'400': 'Произошла ошибка в процессе добавления (запись в лог)',
                   '200': 'Сообщение "Клиент успешно добавлен"'}
    )
    def create(self, request, *args, **kwargs):
        """Создание клиента"""
        if not self.ru.validate_params_to_list(request, self.list_create_params):
            self.logger.error('Тело запроса не прошло валидацию')
            return self.resu.bad_request_response('Данные введены некорректно')
        serialize = ClientSerializer(data=request.data)
        if serialize.is_valid(raise_exception=True):
            if ClientActionClass().action(CREATE, serialize.data) is SUCCESS:
                self.logger.info('Клиент добавлен')
                return self.resu.ok_response('Клиент успешно добавлен')
            self.logger.error(f'Ошибка при добавлении клиента')
            return self.resu.sorry_try_again_response()
        self.logger.error(f'Ошибка сериализации данных клиента при добавлении: {ExceptionHandling.get_traceback()}')
        return self.resu.sorry_try_again_response()

    @swagger_auto_schema(
        tags=['Клиенты', ],
        manual_parameters=[
            openapi.Parameter(
                'client_id',
                openapi.IN_QUERY,
                description='ID клиента',
                type=openapi.TYPE_INTEGER,
                required=True
            )
        ],
        request_body=ClientSerializer,
        operation_description="Обновление клиента",
        responses={
            '200': 'Сообщение "Клиент успешно обновлен"',
            '400': 'Произошла ошибка в процессе обновления (запись в лог)',
            '404': 'Не указан идентификатор клиента'
        }
    )
    def update(self, request, *args, **kwargs):
        """Обновление клиента"""
        client_id = self.du.get_int_value_in_dict_by_key('client_id', kwargs)
        if client_id is None:
            self.logger.error('Не получен идентификатор клиента для обновления')
            return self.resu.not_found_response('Не указан идентификатор клиента')
        if not self.ru.validate_params_to_list(request, self.list_create_params):
            self.logger.error('Тело запроса не прошло валидацию')
            return self.resu.bad_request_response('Данные введены некорректно')
        serialize = ClientSerializer(data=request.data)
        if serialize.is_valid(raise_exception=True):
            try:
                full_data = {
                    'client_id': client_id
                }
                full_data.update(serialize.data)
                if ClientActionClass().action(UPDATE, full_data) is SUCCESS:
                    self.logger.info('Клиент обновлен')
                    return self.resu.ok_response('Клиент успешно обновлен')
                self.logger.error(f'Ошибка при обновлении клиента')
                return self.resu.sorry_try_again_response()
            except Exception:
                self.logger.error(f'Ошибка при обновлении данных клиента: {ExceptionHandling.get_traceback()}')
                return self.resu.sorry_try_again_response()
        else:
            self.logger.error(f'Ошибка сериализации данных клиента при обновлении: {ExceptionHandling.get_traceback()}')
            return self.resu.sorry_try_again_response()

    @swagger_auto_schema(
        tags=['Клиенты', ],
        manual_parameters=[
            openapi.Parameter(
                'client_id',
                openapi.IN_QUERY,
                description='ID клиента',
                type=openapi.TYPE_INTEGER,
                required=True
            )
        ],
        operation_description="Удаление клиента",
        responses={
            '200': 'Сообщение "Клиент успешно удален"',
            '400': 'Произошла ошибка в процессе удаления (запись в лог)',
            '404': 'Не указан идентификатор клиента'
        }
    )
    def destroy(self, request, *args, **kwargs):
        """Удаление клиента"""
        client_id = self.du.get_int_value_in_dict_by_key('client_id', kwargs)
        if client_id is None:
            self.logger.error('Не получен идентификатор клиента для удаления')
            return self.resu.not_found_response('Не указан идентификатор клиента')
        try:
            if ClientActionClass().action(DELETE, {'client_id': client_id}) is SUCCESS:
                self.logger.info('Клиент удален')
                return self.resu.ok_response('Клиент успешно удален')
            self.logger.error(f'Ошибка при удалении клиента')
            return self.resu.sorry_try_again_response()
        except Exception:
            self.logger.error(f'Ошибка при удалении клиента: {ExceptionHandling.get_traceback()}')
            return self.resu.sorry_try_again_response()
