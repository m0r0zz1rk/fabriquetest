from django.contrib import admin

from apps.clients.models import Client


@admin.register(Client)
class ClientsAdmin(admin.ModelAdmin):
    """Добавление модели клиентов в административную панель"""
    pass
