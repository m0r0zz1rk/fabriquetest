from django.urls import path

from apps.clients.api.crud_viewset import ClientCRUDViewSet

urlpatterns = [
    path('new/', ClientCRUDViewSet.as_view({'post': 'create'})),
    path('update/<int:client_id>/', ClientCRUDViewSet.as_view({'put': 'update'})),
    path('delete/<int:client_id>/', ClientCRUDViewSet.as_view({'delete': 'destroy'}))
]