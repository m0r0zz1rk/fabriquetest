from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator
from django.db import models

from apps.commons.models import BaseTable
from apps.commons.timezones import TIMEZONES
from apps.commons.validator import PhoneValidators


class Client(BaseTable):
    """Модель клиентов"""
    client_id = models.AutoField(
        primary_key=True,
        editable=False,
        verbose_name='ID клиента'
    )
    phone = models.CharField(
        max_length=11,
        validators=[PhoneValidators().phone_validate, ],
        null=False,
        blank=False,
        unique=True,
        default='70001112223',
        verbose_name='Номер телефона'
    )
    phone_code = models.PositiveIntegerField(
        validators=[MinValueValidator(100), MaxValueValidator(999)],
        null=False,
        blank=False,
        verbose_name='Код мобильного оператора'
    )
    tag = models.CharField(
        max_length=50,
        blank=True,
        verbose_name='Тэг'
    )
    timezone = models.CharField(
        max_length=50,
        choices=TIMEZONES,
        blank=False,
        null=False,
        default='Europe/Moscow',
        verbose_name='Часовой пояс'
    )

    def __str__(self):
        return self.phone

    def clean(self, *args, **kwargs):
        PhoneValidators().phone_validate(self.phone)
        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['phone']
        indexes = [
            models.Index(fields=['phone'])
        ]
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'
