from django.db import models


class BaseTable(models.Model):
    """Базовая модель сущностей"""
    date_create = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Время создания'
    )

    objects = models.Manager()

    class Meta:
        abstract = True
