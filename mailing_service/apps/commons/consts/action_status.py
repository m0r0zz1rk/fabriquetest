SUCCESS = 'success'
ERROR = 'error'

ACTION_STATUS = (
    SUCCESS, 'Успешно',
    ERROR, 'Ошибка'
)
