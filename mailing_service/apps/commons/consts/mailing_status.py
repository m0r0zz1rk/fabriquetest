MAILING_PREPARE = 'mailing_prepare'
MAILING_PROCESS = 'mailing_process'
MAILING_SUCCESS = 'mailing_success'
MAILING_ERROR = 'mailing_error'

MAILING_STATUS = (
    (MAILING_PREPARE, 'Подготовлено к отправке'),
    (MAILING_PROCESS, 'В процессе отправки'),
    (MAILING_SUCCESS, 'Отправлено'),
    (MAILING_ERROR, 'Ошибка отправки'),
)

