CREATE = 'create'
UPDATE = 'update'
DELETE = 'delete'

ACTIONS = (
    (CREATE, 'Создание'),
    (UPDATE, 'Обновление'),
    (DELETE, 'Удаление')
)
