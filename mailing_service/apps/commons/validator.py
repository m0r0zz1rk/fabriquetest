from typing import Union

from django.core.validators import RegexValidator
from django.db.models import CharField


class PhoneValidators:
    """Класс валидации номером телефона"""

    __phone_validator = RegexValidator('7[0-9]{10}', 'Недопустимый формат номера телефона')

    def phone_validate(self, value: Union[str, CharField]):
        """Валидация номера телефона для сохранения в модели клиента"""
        self.__phone_validator(value)
        return True
