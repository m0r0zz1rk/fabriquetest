from abc import ABC, abstractmethod

from apps.commons.consts.action_status import ACTION_STATUS, ERROR, SUCCESS
from apps.commons.consts.actions import ACTIONS, DELETE, CREATE, UPDATE


class BaseActionClass(ABC):
    """Базовый класс действий"""

    base_object = {}
    logger = None
    model = None

    field_id = ''

    get_object_parameter_method = None
    
    def __init__(self):
        """Инициализация класса - установка значений из полученных данных"""
        self._set_model()
        self._set_field_id()
        self._set_get_object_parameter_method()
        self._set_logger()

    @abstractmethod
    def _set_logger(self):
        """Установка журнала"""
        pass

    @abstractmethod
    def _set_model(self):
        """Установка модели"""

    @abstractmethod
    def _set_field_id(self):
        """Установка названия поля ID модели"""
        pass

    @abstractmethod
    def _set_get_object_parameter_method(self):
        """Установка метода получения параметра объекта"""
        pass
    
    @abstractmethod
    def _create_success_message(self) -> str:
        """Сообщение об успешном создании объекта"""
        pass

    @abstractmethod
    def _create_error_message(self) -> str:
        """Сообщение об ошибке при создании объекта"""
        pass

    @abstractmethod
    def _update_no_id_message(self) -> str:
        """Сообщение о непредоставленном ID объекта при обновлении"""
        pass

    @abstractmethod
    def _update_not_found_message(self) -> str:
        """Сообщение о не найденном объекте обновления"""
        pass

    @abstractmethod
    def _update_success_message(self) -> str:
        """Сообщение об успешном обновлении объекта"""
        pass

    @abstractmethod
    def _update_error_message(self) -> str:
        """Сообщение об ошибке при обновлении объекта"""
        pass

    @abstractmethod
    def _delete_success_message(self, data: dict) -> str:
        """Сообщение об успешном удалении объекта"""
        pass

    @abstractmethod
    def _delete_error_message(self) -> str:
        """Сообщение об ошибке при удалении объекта"""
        pass

    @abstractmethod
    def _delete_no_id_message(self) -> str:
        """Сообщение о непредоставленном ID объекта при удалении"""
        pass
    
    def _validate_data(self, data: dict) -> bool:
        """Валидация полученных данных"""
        for key in data.keys():
            if key not in self.base_object.keys():
                return False
        return True

    def _set_data(self, data: dict) -> bool:
        """Запись полученных данных в случае успешной валидации"""
        if not self._validate_data(data):
            self.logger.error(f'Полученные данные не прошли валидацию: {data}')
            return False
        for key in data.keys():
            self.base_object[key] = data[key]
        return True

    def action(self, action: ACTIONS, data: dict) -> ACTION_STATUS:
        """Выполнение действия с полученными данными"""
        if action is not DELETE:
            if not self._set_data(data):
                return ERROR
        if action is CREATE:
            try:
                self.model.objects.create(
                    **self.base_object
                )
                self.logger.info(self._create_success_message())
                return SUCCESS
            except Exception:
                self.logger.error(self._create_error_message())
                return ERROR
        elif action is UPDATE:
            if self.base_object[self.field_id] is None:
                self.logger.error(self._update_no_id_message())
                return ERROR
            try:
                date_create = self.get_object_parameter_method(
                    self.base_object[self.field_id],
                    'date_create'
                )
                if date_create is None:
                    self.logger.error(self._update_not_found_message())
                    return ERROR
                self.base_object['date_create'] = date_create
                get_field_id = {
                    self.field_id: self.base_object[self.field_id]
                }
                del self.base_object[self.field_id]
                self.model.objects.filter(**get_field_id).update(**self.base_object)
                self.logger.info(self._update_success_message())
                return SUCCESS
            except Exception:
                self.logger.error(self._update_error_message())
                return ERROR
        else:
            if self.field_id in data.keys():
                try:
                    get_value = {
                        self.field_id: data[self.field_id]
                    }
                    self.model.objects.get(**get_value).delete()
                    self.logger.info(self._delete_success_message(data))
                    return SUCCESS
                except Exception:
                    self.logger.error(self._delete_error_message())
                    return ERROR
            else:
                self.logger.error(self._delete_no_id_message())
                return ERROR
