class ListUtils:
    """Методы и валидаторы при работе со списками"""

    @staticmethod
    def is_list_empty(l: list) -> bool:
        """Проверка на пустой лист"""
        return len(l) == 0

    @staticmethod
    def is_dict_keys_valid_list(data: dict, value_list: list) -> bool:
        """Проверка на полное совпадение значений ключей словаря и списка"""
        for k in data.keys():
            if k not in value_list:
                return False
        return True

    @staticmethod
    def is_el_in_list(l: list, el: object) -> bool:
        """Проверка на наличие объекта в списке"""
        return el in l
