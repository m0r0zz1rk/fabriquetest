from django.core.mail import send_mail

from apps.commons.consts.mailing_status import MAILING_STATUS
from apps.commons.django import DjangoUtils
from apps.mailings.utils.mailing_utils import MailingUtils
from webapp.init_celery import app


@app.task
def send_email_stats_task():
    """Запуск задания в Celery на отправку email со статистикой по обработанным рассылкам"""
    stat_dict = MailingUtils.get_main_statistics(for_email=True)
    message = f'\nКоличество обработанных рассылок: {stat_dict["mailings_count"]}\n'
    for status in MAILING_STATUS:
        message += f'Количество сообщений со статусом "{status[1]}": {stat_dict[status[0]]}\n'
    recipients = DjangoUtils().get_parameter_from_settings('EMAIL_RECIPIENT_LIST')
    if len(recipients) == 0:
        recipients = DjangoUtils.get_admin_emails_list()
    send_mail(
        subject='Тестовый сервис: Статистика по обработанным рассылкам',
        message=message,
        from_email=DjangoUtils().get_parameter_from_settings('EMAIL_HOST_USER'),
        recipient_list=recipients
    )
