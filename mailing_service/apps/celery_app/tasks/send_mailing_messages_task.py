from apps.mailings.service.mailings_actions.ExternalServiceClass import ExternalServiceAction
from webapp.init_celery import app


@app.task
def send_mailing_messages_task(mailing_id: int):
    """Запуск задания в Celery на отправку сообщений рассылки"""
    ExternalServiceAction(mailing_id).mailing()
