from apps.mailings.service.mailings_actions.CheckStartMailingClass import CheckStartMailingsAction
from webapp.init_celery import app


@app.task
def check_and_start_mailing_task():
    """Запуск задания в Celery на проверку времени начала и запуск подходящих рассылок"""
    CheckStartMailingsAction().start_mailings(False)
