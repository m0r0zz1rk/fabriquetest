from django.apps import AppConfig


class MailingsMessagesConfig(AppConfig):
    name = 'apps.mailings_messages'
    verbose_name = 'Приложение сообщений рассылок'

