import datetime

from django.db import models

from apps.clients.models import Client
from apps.commons.consts.mailing_status import MAILING_STATUS
from apps.commons.models import BaseTable
from apps.mailings.models import Mailing


class Message(BaseTable):
    """Модель сообщений рассылки"""
    message_id = models.AutoField(
        primary_key=True,
        editable=False,
        verbose_name='ID сообщения'
    )
    mailing_time = models.DateTimeField(
        auto_now=True,
        verbose_name='Дата и время создания (отправки)'
    )
    mailing_status = models.CharField(
        max_length=30,
        choices=MAILING_STATUS,
        verbose_name='Статус отправки'
    )
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.SET_NULL,
        null=True,
        default=None,
        verbose_name='Рассылка'
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.SET_NULL,
        null=True,
        default=None,
        verbose_name='Клиент'
    )

    def __str__(self):
        label = f'Сообщение от {self.mailing_time.strftime("%d.%m.%Y %H:%M")}'
        if self.mailing is not None:
            label += f' с сообщением "{self.mailing.message_text}"'
        if self.client is not None:
            label += f' для клиента {self.client}'
        return label

    class Meta:
        ordering = ['-mailing_time']
        indexes = [
            models.Index(fields=['mailing_status', 'mailing', 'client'])
        ]
        verbose_name = 'Сообщение рассылки'
        verbose_name_plural = 'Сообщения рассылок'
