from django.contrib import admin

from apps.mailings_messages.models import Message


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    """Отображение модели сообщений рассылок в административной панели"""
    list_display = ('mailing_time', 'mailing_status', 'mailing', 'client')
