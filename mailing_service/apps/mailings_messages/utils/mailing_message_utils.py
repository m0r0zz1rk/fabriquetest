from typing import Optional

from apps.commons.consts.mailing_status import MAILING_STATUS
from apps.mailings_messages.models import Message


class MailingMessageUtils:
    """Класс методов для работы с сообщениями рассылок"""

    @staticmethod
    def check_mailing_message_by_message_id(message_id: int) -> bool:
        """Проверка на существующее сообщение рассылки по полученному message_id"""
        return Message.objects.filter(message_id=message_id).exists()

    def get_mailing_message_attribute_by_message_id(self, message_id: int, parameter: str) -> Optional[str]:
        """Получение атрибута сообщения рассылки по message_id"""
        if self.check_mailing_message_by_message_id(message_id):
            try:
                return getattr(Message.objects.get(message_id=message_id), parameter)
            except:
                pass
        return None

    def set_mailing_message_status(self, message_id: int, status: MAILING_STATUS) -> None:
        """Установка статуса сообщению рассылки по полученному message_id"""
        if self.check_mailing_message_by_message_id(message_id):
            mess = Message.objects.get(message_id=message_id)
            mess.mailing_status = status
            mess.save(update_fields=['mailing_status'])
        return None
