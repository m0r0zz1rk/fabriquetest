from .swg import *

DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    },
}

TEST_URL = 'http://localhost:8000'

#В случае, если будет фронт
CORS_ALLOWED_ORIGINS = ['http://localhost:8080', 'http://localhost:5173']