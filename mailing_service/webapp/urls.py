from django.contrib import admin
from django.urls import path, include

from .vars.yasg import urlpatterns as yasg_urlpatterns

api_version = '/api/v1/'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/clients/', include('apps.clients.urls')),
    path('api/v1/mailings/', include('apps.mailings.urls'))
]

urlpatterns += yasg_urlpatterns

